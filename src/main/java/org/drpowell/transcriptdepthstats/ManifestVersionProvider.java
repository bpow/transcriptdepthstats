package org.drpowell.transcriptdepthstats;

import picocli.CommandLine;

public class ManifestVersionProvider implements CommandLine.IVersionProvider {

    @Override
    public String[] getVersion() throws Exception {
        String [] version = new String[1];
        version[0] = this.getClass().getPackage().getImplementationVersion();
        if (version[0] == null) version[0] = "DEVELOPMENT";
        return version;
    }
}
