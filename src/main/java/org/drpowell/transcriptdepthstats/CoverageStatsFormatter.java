package org.drpowell.transcriptdepthstats;

import htsjdk.samtools.util.Interval;

import java.io.PrintStream;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public abstract class CoverageStatsFormatter {
    static final Logger logger = Logger.getLogger(CoverageStatsFormatter.class.getName());
    static final DecimalFormat df2;
    final boolean nonCoding;
    final PrintStream printStream;

    static {
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setNaN("NaN");
        dfs.setInfinity("Inf");
        df2 = new DecimalFormat("#.##", dfs);
    }

    abstract CoverageStatsFormatter addStats(GffTranscript transcript, List<CalculateDepthStatsFromGff.DepthStats> stats);

    public CoverageStatsFormatter(boolean nonCoding, PrintStream printStream) {
        this.nonCoding = nonCoding;
        this.printStream = printStream;
    }

    void close() {
        printStream.close();
    }

    static class TSVLinePerTranscriptByExonsFormatter extends CoverageStatsFormatter {
        public TSVLinePerTranscriptByExonsFormatter(boolean nonCoding, PrintStream printStream) {
            super(nonCoding, printStream);
            // Header
            printStream.println(String.join("\t",
                    "#gene", "transcript", "protein_id", "sequence", nonCoding? "exons" : "intervals", "coverage_mean", "coverage_stdev",
                    "coverage_min", "coverage_quartile1", "coverage_median", "coverage_quartile3", "coverage_max",
                    "pct_covered_at_thresholds"));
        }

        public CoverageStatsFormatter addStats(GffTranscript transcript, List<CalculateDepthStatsFromGff.DepthStats> stats) {
            List<Interval> intervals = nonCoding? transcript.exons : transcript.cds;
            printStream.println(String.join("\t"
                    , transcript.gene
                    , transcript.transcript_id
                    , transcript.protein_id
                    , transcript.interval.getContig()
                    //, transcript.isForwardStrand ? "+" : "-"
                    , transcript.intervalsString(intervals)

                    // FIXME: this is ugly and inefficient....
                    , stats.stream().map(s -> df2.format(s.mean)).collect(Collectors.joining(","))
                    , stats.stream().map(s -> df2.format(s.stdev)).collect(Collectors.joining(","))
                    , stats.stream().map(s -> df2.format(s.min)).collect(Collectors.joining(","))
                    , stats.stream().map(s -> df2.format(s.quartiles[0])).collect(Collectors.joining(","))
                    , stats.stream().map(s -> df2.format(s.quartiles[1])).collect(Collectors.joining(","))
                    , stats.stream().map(s -> df2.format(s.quartiles[2])).collect(Collectors.joining(","))
                    , stats.stream().map(s -> df2.format(s.max)).collect(Collectors.joining(","))

                    , stats.stream().map(s -> Arrays.stream(s.pctCovered)
                            .mapToObj(df2::format)
                            .collect(Collectors.joining(",")))
                            .collect(Collectors.joining(";"))
            ));
            return this;
        }
    }

    static class TSVLinePerTranscriptFormatter extends CoverageStatsFormatter {
        public TSVLinePerTranscriptFormatter(boolean nonCoding, int [] depths, PrintStream printStream) {
            super(nonCoding, printStream);
            ArrayList<String> headers = new ArrayList<>();
            headers.addAll(Arrays.asList("#transcript_id", "protein_id", "gene",
                    "coverage_mean", "coverage_stdev", "coverage_min", "coverage_quartile1", "coverage_median", "coverage_quartile3", "coverage_max"));
            for (int depth : depths) {
                headers.add("pct_gte_" + depth);
            }
            printStream.println(String.join("\t", headers));
        }

        @Override
        public CoverageStatsFormatter addStats(GffTranscript transcript, List<CalculateDepthStatsFromGff.DepthStats> stats) {
            if (stats.size() != 1) {
                logger.severe(String.format("by-transcript stats should have one (and only one) DepthStats object, found %d for transcript %s",
                        stats.size(), transcript.transcript_id));
            }
            stats.stream().forEach(stat ->
                printStream.println(String.join("\t",
                        transcript.transcript_id, transcript.protein_id, transcript.gene,
                        df2.format(stat.mean), df2.format(stat.stdev), Integer.toString(stat.min),
                        df2.format(stat.quartiles[0]), df2.format(stat.quartiles[1]), df2.format(stat.quartiles[2]),
                        Integer.toString(stat.max),
                        Arrays.stream(stat.pctCovered)
                                .mapToObj(df2::format)
                                .collect(Collectors.joining("\t")))));
            return this;
        }
    }

    static class TSVLinePerRegionFormatter extends CoverageStatsFormatter {
        public TSVLinePerRegionFormatter(boolean nonCoding, int [] depths, PrintStream printStream) {
            super(nonCoding, printStream);
            ArrayList<String> headers = new ArrayList<>();
            headers.addAll(Arrays.asList("#sequence", "start0", "end", "transcript_id", "protein_id", "gene", "region_index",
                    "coverage_mean", "coverage_stdev", "coverage_min", "coverage_quartile1", "coverage_median", "coverage_quartile3", "coverage_max"));
            for (int depth : depths) {
                headers.add("pct_gte_" + depth);
            }
            // Header
            printStream.println(String.join("\t", headers));
        }

        public CoverageStatsFormatter addStats(GffTranscript transcript, List<CalculateDepthStatsFromGff.DepthStats> stats) {
            List<Interval> intervals = nonCoding ? transcript.exons : transcript.cds;
            final int numIntervals = intervals.size();
            final boolean reverse = transcript.interval.isNegativeStrand();
            for (int i = 0; i < intervals.size(); i++) {
                Interval interval = intervals.get(i);
                CalculateDepthStatsFromGff.DepthStats stat = stats.get(i);
                printStream.println(String.join("\t"
                        , interval.getContig()
                        , Integer.toString(interval.getStart() - 1)
                        , Integer.toString(interval.getEnd())
                        , transcript.transcript_id
                        , transcript.protein_id
                        , transcript.gene
                        , Integer.toString(reverse ? numIntervals - i - 1 : i)
                        , df2.format(stat.mean)
                        , df2.format(stat.stdev)
                        , Integer.toString(stat.min)
                        , df2.format(stat.quartiles[0])
                        , df2.format(stat.quartiles[1])
                        , df2.format(stat.quartiles[2])
                        , Integer.toString(stat.max)
                        , Arrays.stream(stat.pctCovered)
                                .mapToObj(df2::format)
                                .collect(Collectors.joining("\t"))
                ));
            }
            return this;
        }
    }

    static class JSONLinesFormatter extends CoverageStatsFormatter {

        public JSONLinesFormatter(boolean nonCoding, PrintStream printStream) {
            super(nonCoding, printStream);
        }

        private StringBuilder appendKeyValue(StringBuilder buffer, String before, String key, String value) {
            return buffer.append(before).append("\"").append(key).append("\":\"").append(value).append("\"");
        }

        private StringBuilder appendKeyValue(StringBuilder buffer, String before, String key, Number value) {
            return buffer.append(before).append("\"").append(key).append("\":").append(df2.format(value));
        }

        public CoverageStatsFormatter addStats(GffTranscript transcript, List<CalculateDepthStatsFromGff.DepthStats> stats) {
            List<Interval> intervals = nonCoding ? transcript.exons : transcript.cds;
            StringBuilder sb = new StringBuilder();
            appendKeyValue(sb, "{", "transcript_id", transcript.transcript_id);
            appendKeyValue(sb, ",", "gene", transcript.gene);
            appendKeyValue(sb, ",", "protein_id", transcript.gene);
            appendKeyValue(sb, ",", "chrom", transcript.interval.getContig());
            appendKeyValue(sb, ",", "strand", transcript.interval.isNegativeStrand()? "-" : "+");
            sb.append(nonCoding? ",\"exons\":" : ",\"cds\":").append("[");
            for (int i = 0; i < intervals.size(); i++) {
                Interval interval = intervals.get(i);
                CalculateDepthStatsFromGff.DepthStats stat = stats.get(i);
                appendKeyValue(sb, "{", "start0", interval.getStart()-1);
                appendKeyValue(sb, ",", "end0", interval.getEnd());
                appendKeyValue(sb, ",", "mean", stat.mean);
                appendKeyValue(sb, ",", "stdev", stat.stdev);
                appendKeyValue(sb, ",", "min", stat.min);
                appendKeyValue(sb, ",", "q25", stat.quartiles[0]);
                appendKeyValue(sb, ",", "median", stat.quartiles[1]);
                appendKeyValue(sb, ",", "q75", stat.quartiles[2]);
                appendKeyValue(sb, ",", "max", stat.max);
                for (int d = 0; d < stat.depthThresholds.length; d++) {
                    appendKeyValue(sb, ",", "pct_at_least_" + Integer.toString(stat.depthThresholds[d]), stat.pctCovered[d]);
                }
                sb.append("},");
            }
            sb.deleteCharAt(sb.length()-1).append("]}"); // remove the trailing comma after last item in list
            printStream.println(sb);
            return this;
        }
    }

}
