package org.drpowell.transcriptdepthstats;

import htsjdk.samtools.util.Interval;

import java.io.*;
import java.net.URLDecoder;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.zip.GZIPInputStream;

public class CodingTranscriptsFromGff implements Callable<List<GffTranscript>> {
    private final File inputFile;
    private final Map<String, GffTranscript> transcripts = new LinkedHashMap<>();

    public CodingTranscriptsFromGff(File inputFile) {
        this.inputFile = inputFile;
    }

    private static String [] gffURLDecode(final String input) {
        // encode '+' now since GFF does not encode them, (but URLDecoder would change them to spaces)
        String [] output = input.replace("+", "%2B").split(",");
        for (int i = 0; i < output.length; i++) {
            try {
                output[i] = URLDecoder.decode(output[i], "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return output;
    }

    private static Map<String, String[]> decodeAttributes(final String joined) {
        LinkedHashMap<String, String[]> decoded = new LinkedHashMap<>();
        Arrays.stream(joined.split(";"))
                .map( item -> item.split("=", 2))
                .forEach( kv -> decoded.put(kv[0], gffURLDecode(kv[1])));
        return decoded;
    }

    @Override
    public List<GffTranscript> call() throws Exception {
        InputStream is = new FileInputStream(inputFile);
        if (inputFile.getName().endsWith(".gz")) {
            is = new GZIPInputStream(is);
        }
        List<String> includedTypes = Arrays.asList("mRNA", "CDS", "exon");
        new BufferedReader(new InputStreamReader(is)).lines().filter(line -> !line.startsWith("#")).forEach(line -> {
            final String [] row = line.split("\\t");
            final String gffType = row[2];
            // columns are "seqid source type start end score strand phase attributes"
            if (row[1].startsWith("BestRefSeq") && includedTypes.contains(gffType)) {
                Map<String, String []> attributes = decodeAttributes(row[8]);
                Interval interval = new Interval(row[0], Integer.parseInt(row[3]), Integer.parseInt(row[4]), "-".equals(row[6]), null);
                if (gffType.equals("mRNA")) {
                    final String transcriptId = attributes.get("transcript_id")[0];
                    final GffTranscript transcript = new GffTranscript(
                            interval, transcriptId, attributes.get("gene")[0], attributes.get("product")[0]);
                    transcripts.put(attributes.get("ID")[0], transcript);
                } else if (gffType.equals("CDS")) {
                    final GffTranscript transcript = transcripts.get(attributes.get("Parent")[0]);
                    transcript.cds.add(interval);
                    transcript.protein_id = attributes.get("protein_id")[0];
                    if (transcript.cds_note.isEmpty() && attributes.containsKey("Note")) {
                        transcript.cds_note = attributes.get("Note")[0];
                    }
                } else if (gffType.equals("exon")) {
                    final GffTranscript transcript = transcripts.get(attributes.get("Parent")[0]);
                    // only continue for exons where parent is remembered (this avoids non-coding transcripts, which don't have type "mRNA")
                    if (transcript != null) {
                        transcript.exons.add(interval);
                        if (transcript.exon_note.isEmpty() && attributes.containsKey("Note")) {
                            transcript.exon_note = attributes.get("Note")[0];
                        }
                    }
                }
            }
        });
        return new ArrayList<>(transcripts.values());
    }

    public static void main(String... args) throws Exception {
        long startTime = System.currentTimeMillis();
        Collection<GffTranscript> transcripts = new CodingTranscriptsFromGff(new File(args[0])).call();
        System.err.println(String.format("Took %.02f seconds", (System.currentTimeMillis()-startTime)/1000f));

//        System.out.println("#gene\ttranscript\tcontig\tstrand\tproduct\tcds_note\tcds\texons_note\texons");
//        for (GffTranscript t: transcripts) {
//            System.out.println(t);
//        }
        for (GffTranscript t: transcripts) {
            int i = 0;
            for (Interval cds: t.cds) {
                System.out.println(String.join("\t",
                        cds.getContig(), Integer.toString(cds.getStart()-1), Integer.toString(cds.getEnd()),
                        t.gene, t.transcript_id
                        //, String.format("c_%02d", i)
                        ));
                i++;
            }
//            for (Interval exon: t.exons) {
//                System.out.println(String.join("\t",
//                        exon.getContig(), Integer.toString(exon.getStart()-1), Integer.toString(exon.getEnd()),
//                        t.gene, t.transcript_id, String.format("c_%02d", i)));
//                i++;
//            }
        }
    }
}
