package org.drpowell.transcriptdepthstats;

import htsjdk.samtools.*;
import htsjdk.samtools.util.BlockCompressedOutputStream;
import htsjdk.samtools.util.Interval;
import htsjdk.samtools.util.IntervalTreeMap;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.Collectors;


@Command(name="transcriptDepthStats",
        description="Calculate sequence depth summary statistics for regions of a genome, based on a GFF file",
        versionProvider = org.drpowell.transcriptdepthstats.ManifestVersionProvider.class,
        mixinStandardHelpOptions = true,
        separator = " ",
        showDefaultValues = true,
        sortOptions = false)
public class CalculateDepthStatsFromGff implements Runnable {
    private Logger logger = Logger.getLogger(CalculateDepthStatsFromGff.class.getName());

    // maybe try alternative input file in BED format, with "seq start end gene;transcr;strand;{e|c}_%02d"
    // or just "bed-like" with first 3 columns being BED representation, others tab-delimited as needed

    @Option(names = {"-g", "--gff"}, required = true, arity = "1",
            description = {"Input intervals in GFF3 format (as from NCBI).",
                           "May be GZIP-compressed (with '.gz' extension) or /dev/stdin"})
    private
    File gffFile;

    @Option(names = {"-b", "--bam"}, required = true, arity = "1", description = "Input sequence read (bam) file")
    File bamFile;

    @Option(names = {"-n", "--non-coding"}, required = false, arity = "1",
            description = "Include non-coding portions of exons (by default, depths are only counted in CDS")
    boolean nonCoding;

    @Option(names = {"-m", "--mapping-quality"},
            description = "Minimum mapping quality to consider a read (inclusive)")
    Integer mappingQualityThreshold = 20;

    @Option(names = {"-q", "--base-quality"},
            description = "Minimum base quality to consider a read (inclusive)")
    Integer baseQualityThreshold = 20;

    @Option(names = {"-d", "--depth"},
            description = "Minimum depth to report for percent covered")
    int [] depthThresholds = {1, 5, 10, 20, 50};

    @Option(names = {"-r", "--remember"},
            description = "Remember previously-calculated regions (in case they show up again, for instance " +
                    "in a list of exons with multiple transcripts of the same gene). This significantly speeds up " +
                    "processing for the NCBI refseq transcripts, at the cost of memory.")
    boolean remember = false;

    @Option(names = {"-e", "--exons"}, required = false, arity = "1",
            description = "Output file of coverage by exons (use '-' for stdout")
    String exonsOutFilename = "-";

    @Option(names = {"-t", "--transcripts"}, required = false, arity = "1",
            description = "Output file of coverage aggregated by transcripts (use '-' for stdout")
    String transcriptsOutFilename = null;

    class DepthStats {
        final int min;
        final int max;
        final double [] quartiles;
        final double mean;
        final double stdev;
        final int length;
        final int [] numCovered;
        final double [] pctCovered;
        final int [] depthThresholds;

        public DepthStats(int [] depths, int [] depthThresholdCutoffs) {
            depthThresholds = depthThresholdCutoffs;
            double [] doubles = Arrays.stream(depths).asDoubleStream().toArray();
            // FIXME - commons.math is pretty heavy for this (~3mb), but it works...
            DescriptiveStatistics stats = new DescriptiveStatistics(doubles);
            min = (int) stats.getMin();
            max = (int) stats.getMax();
            quartiles = new double[] {stats.getPercentile(25), stats.getPercentile(50), stats.getPercentile(75)};
            mean = stats.getMean();
            stdev = stats.getStandardDeviation();
            length = depths.length;
            numCovered = new int[depthThresholds.length];
            for (int i = 0; i < length; i++) {
                for (int j = 0; j < numCovered.length; j++) {
                    if (depths[i] >= depthThresholds[j]) {
                        numCovered[j]++;
                    }
                }
            }
            pctCovered = Arrays.stream(numCovered).mapToDouble(i -> 100 * (double) i / length).toArray();
        }

        public String toString() {
            return String.format("min=%d;max=%d;thresholds=%s;num_covered=%s;pct_covered=%s;quartiles=%s;mean=%.1f;stdev=%.1f",
                    min, max,
                    Arrays.stream(depthThresholds).boxed().map(i -> Integer.toString(i)).collect(Collectors.joining(",")),
                    Arrays.stream(numCovered).boxed().map(n -> Integer.toString(n)).collect(Collectors.joining(",")),
                    Arrays.stream(pctCovered).boxed().map(p -> String.format("%.1f", p)).collect(Collectors.joining(",")),
                    Arrays.stream(quartiles).boxed().map(q -> String.format("%.1f", q)).collect(Collectors.joining(",")),
                    mean, stdev);
        }
    }

    public static int[] depthsInRegion(final SamReader reader, Interval interval,
                                       int mappingQualityThreshold, int baseQualityThreshold) {
        final int [] depths = new int[interval.length()];
        final int oneBasedStart = interval.getStart();
        final int end = interval.getEnd();

        SAMRecordIterator sri = reader.query(interval.getContig(), interval.getStart(), interval.getEnd(), false);
        SAMRecord record;
        while (sri.hasNext()) {
            record = sri.next();
            if (record.getMappingQuality() >= mappingQualityThreshold) {
                byte[] baseQualities = record.getBaseQualities();
                for (int i = 0; i < record.getReadLength(); i++) {
                    if (baseQualities[i] >= baseQualityThreshold) {
                        // careful with coordinates, since htsjdk is one-based
                        int refPos = record.getReferencePositionAtReadPosition(i);
                        if (refPos >= oneBasedStart && refPos <= end) {
                            depths[refPos - oneBasedStart]++;
                        }
                    }
                }
            }
        }
        sri.close();
        return depths;
    }

    private List<int []> depthsForIntervals(Collection<Interval> intervals, SamReader reader, IntervalTreeMap<int[]> rememberedDepths) {
        return intervals.stream().map(interval -> {
            if (remember && rememberedDepths.containsKey(interval)) return rememberedDepths.get(interval);
            int[] depths = depthsInRegion(reader, interval, mappingQualityThreshold, baseQualityThreshold);
            if (remember) {
                rememberedDepths.put(interval, depths);
            }
            return depths;
        }).collect(Collectors.toList());
    }

    private List<DepthStats> statsForIntervals(Collection<Interval> intervals, SamReader reader, IntervalTreeMap<int []> rememberedDepths) {
        return depthsForIntervals(intervals, reader, rememberedDepths).stream()
                .map(depths -> new DepthStats(depths, depthThresholds)).collect(Collectors.toList());
    }

    private List<DepthStats> statsForIntervalsrememberingStats(Collection<Interval> intervalsToAnalyze, SamReader reader, IntervalTreeMap<DepthStats> rememberedStats) {
        return intervalsToAnalyze.stream().map(interval -> {
            if (remember && rememberedStats.containsKey(interval)) return rememberedStats.get(interval);
            int[] depths = depthsInRegion(reader, interval, mappingQualityThreshold, baseQualityThreshold);
            DepthStats stats = new DepthStats(depths, depthThresholds);
            if (remember) {
                rememberedStats.put(interval, stats);
            }
            return stats;
        }).collect(Collectors.toList());
    }

    private int[] combineIntArrays(List<int []> arrays) {
        int length = arrays.stream().mapToInt(a -> a.length).sum();
        int [] output = new int[length];
        int offset = 0;
        for (int [] a: arrays) {
            System.arraycopy(a, 0, output, offset, a.length);
            offset += a.length;
        }
        return output;
    }

    private PrintStream streamForFilename(String filename) throws IOException {
        if ("-".equals(filename)) return System.out;
        if (filename.endsWith(".gz")) {
            return new PrintStream(new BlockCompressedOutputStream(filename));
        }
        return new PrintStream(new FileOutputStream(new File(filename)));
    }

    @Override
    public void run() {
        final SamReader reader = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT).open(bamFile);

        // TODO - check if a simple HashMap would be smaller/faster (since just testing for equality)
        IntervalTreeMap<int []> previousDepths = new IntervalTreeMap<>();

        try {
            final Optional<CoverageStatsFormatter> perExonFormatter =
                    exonsOutFilename == null
                            ? Optional.empty()
                            : Optional.of(new CoverageStatsFormatter.TSVLinePerRegionFormatter(nonCoding, depthThresholds, streamForFilename(exonsOutFilename)));
            final Optional<CoverageStatsFormatter> perTranscriptFormatter =
                    exonsOutFilename == null
                            ? Optional.empty()
                            : Optional.of(new CoverageStatsFormatter.TSVLinePerTranscriptFormatter(nonCoding, depthThresholds, streamForFilename(transcriptsOutFilename)));
            for (GffTranscript transcript : new CodingTranscriptsFromGff(gffFile).call()) {
                if (reader.getFileHeader().getSequence(transcript.interval.getContig()) == null) {
                    // FIXME- arguably, these should probably be output as not having coverage
                    logger.warning(String.format("Skipping gene '%s' transcript '%s', which occurs on contig '%s' that is not in bam file",
                            transcript.gene, transcript.transcript_id, transcript.interval.getContig()));
                } else {
                    List<Interval> intervals = nonCoding ? transcript.exons : transcript.cds;
                    List<int []> depthLists = depthsForIntervals(intervals, reader, previousDepths);
                    int [] transcriptDepths = combineIntArrays(depthLists);
                    perExonFormatter.ifPresent(f ->
                            f.addStats(transcript, depthLists.stream().map(depthList -> new DepthStats(depthList, depthThresholds)).collect(Collectors.toList())));
                    perTranscriptFormatter.ifPresent(f ->
                            f.addStats(transcript, Arrays.asList(new DepthStats(transcriptDepths, depthThresholds))));
                }
            }
            perExonFormatter.ifPresent(f -> f.close());
            perTranscriptFormatter.ifPresent(f -> f.close());

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String... args) {
        CommandLine.run(new CalculateDepthStatsFromGff(), args);
    }
}
