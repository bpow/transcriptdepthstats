package org.drpowell.transcriptdepthstats;

import htsjdk.samtools.util.Interval;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

class GffTranscript {
    public final Interval interval;
    public final String transcript_id, gene, product;
    // poorly encapsulated, but this is basically just a struct, so I'm not going to worry about it
    public String protein_id = "", cds_note = "", exon_note = "";
    public final ArrayList<Interval> cds = new ArrayList<>();
    public final ArrayList<Interval> exons = new ArrayList<>();

    public GffTranscript(Interval interval, String transcript_id, String gene, String product) {
        this.interval = interval;
        this.transcript_id = transcript_id;
        this.gene = gene;
        this.product = product;
    }

    public String intervalsString(List<Interval> intervals) {
        return intervals.stream()
                .map(interval -> String.format("%d,%d", interval.getStart()-1, interval.getEnd()))
                .collect(Collectors.joining(";"));
    }

    public String tabDelimited() {
        return String.join("\t", interval.getContig(),
                Integer.toString(interval.getStart()-1), Integer.toString(interval.getEnd()),
                gene, transcript_id, protein_id, product,
                cds_note, intervalsString(cds),
                exon_note, intervalsString(exons));
    }
}
