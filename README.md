Generate summary statistics regarding coverage of CDS or exon regions.

# Building

Invoke gradle with the 'jar' target:

```
./gradlew jar
```

This should produce a `transcriptDepthStats.jar` file in the root directory of this repository.

# Requirements

- java 8 or later
- a GFF3 file to use as a datasource (for instance, from [NCBI's reference](ftp://ftp.ncbi.nih.gov/genomes/refseq/vertebrate_mammalian/Homo_sapiens/all_assembly_versions/GCF_000001405.26_GRCh38/GCF_000001405.26_GRCh38_genomic.gff.gz)
- a BAM file of interest to you (must be aligned against the reference used in the GFF3 file)

# Running

`java -jar transcriptDepthStats.jar --help` will give you the options.
